package com.mjk.springular.v1.user.model;

import com.mjk.springular.v1.base.model.BaseModel;

public class User extends BaseModel {
	
	private String userName;
	
	private String passwordHash;
	
	private String emailId;
	
	private String recoveryEmailId;
	
	private boolean isVerified;
	
	public User() {
		super();
	}

	public User(String userName, String passwordHash, String emailId, String recoveryEmailId) {
		super();
		this.userName = userName;
		this.passwordHash = passwordHash;
		this.emailId = emailId;
		this.recoveryEmailId = recoveryEmailId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPasswordHash() {
		return this.passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getRecoveryEmailId() {
		return recoveryEmailId;
	}

	public void setRecoveryEmailId(String recoveryEmailId) {
		this.recoveryEmailId = recoveryEmailId;
	}

	public boolean isVerified() {
		return isVerified;
	}

	public void setVerified(boolean isVerified) {
		this.isVerified = isVerified;
	}
	
}
