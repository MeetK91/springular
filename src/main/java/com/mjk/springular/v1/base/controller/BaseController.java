package com.mjk.springular.v1.base.controller;

import org.springframework.web.bind.annotation.GetMapping;

public abstract class BaseController {

	public static final String API_VERSION = "/v1";
	public BaseController() {
		// TODO Auto-generated constructor stub
	}
	
	@GetMapping(path= {"/", API_VERSION, API_VERSION+"/connect"})
	public boolean isConnected() {
		return true;
	}

}
