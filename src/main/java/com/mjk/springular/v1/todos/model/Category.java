package com.mjk.springular.v1.todos.model;

import com.mjk.springular.v1.base.model.BaseModel;
import com.mjk.springular.v1.user.model.User;

public class Category extends BaseModel {
	
	private String name;
	
	private String colorCode;
	
	private User user;

	public Category() {
		super();
	}

	public Category(String name, String colorCode, User user) {
		super();
		this.name = name;
		this.colorCode = colorCode;
		this.user = user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColorCode() {
		return colorCode;
	}

	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
}