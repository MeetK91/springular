package com.mjk.springular.v1.todos.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.mjk.springular.v1.base.controller.BaseController;
import com.mjk.springular.v1.todos.model.HelloBean;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class HelloWorldController extends BaseController {
	
	@GetMapping(path= {API_VERSION+"/todos",API_VERSION+"/todos/hello"})
	public HelloBean hello() {
		
//		throw new RuntimeException("Oops! Error in hello().");
		
		return new HelloBean("Hello World");
	}
	
	@GetMapping(path= {API_VERSION+"/todos/hello/{name}"})
	public HelloBean helloBean(@PathVariable String name) {
		
		System.out.println("------LOG BEFORE RUNTIMEEXCEPTION------");
				throw new RuntimeException("Oops! Error in helloBean(name)."); //TODO - Solve CORS error on throwing Exception
//		return new HelloBean(name.equals("")?"Hello -":String.format("Hello %s", name));
	}
}
