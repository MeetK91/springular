package com.mjk.springular.v1.todos.model;

import java.util.Date;

import com.mjk.springular.v1.base.model.BaseModel;
import com.mjk.springular.v1.user.model.User;

public class Todo extends BaseModel {

	private String todoText;
	
	private Date dueDate;
	
	private User user;
	
	private boolean isCompleted;
	
	private Date completionDate;
	
	private int priority;

	//TODO - Implement recurrence and reminder engine
	//private Date reminder;
	//private int reminderFrequency
	//private int recurrence
	
	public String getTodoText() {
		return todoText;
	}

	public void setTodoText(String todoText) {
		this.todoText = todoText;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isCompleted() {
		return isCompleted;
	}

	public void setCompleted(boolean isCompleted) {
		this.isCompleted = isCompleted;
	}

	public Date getCompletionDate() {
		return completionDate;
	}

	public void setCompletionDate(Date completionDate) {
		this.completionDate = completionDate;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}
	
}